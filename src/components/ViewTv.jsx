import React, { useEffect, useState } from "react";
import axios from "axios";

const ViewMovie = () => {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    axios
      .get(`https://api.themoviedb.org/3/tv/popular/`, {
        params: {
          api_key: "24421cafe490cc00aeb89c9771051f13",
        },
      })
      .then((response) => {
        setMovies(response.data.results);
      });
  }, []);

  return (
    <>
      <div className="relative overflow-hidden py-10 pt-10 p-10">
        <div className="pt-28 pb-20 text-center items-center">
          <h1 className="text-white text-xl lg:text-4xl font-semibold text-center">
            Popular TV
          </h1>
        </div>
        <div className="grid grid-cols-4 gap-3 pb-10 items-center justify-center w-full">
          {movies.map((movie) => (
            <div className="w-full ">
              <div className="hover:scale-95 transition relative overflow-hidden pb-10">
                <img
                  src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`}
                  alt=""
                  className="w-full h-full"
                />
                <h2 className="text-white truncate capitalize text-xs w-52 pt-5">
                  {movie.name}
                </h2>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default ViewMovie;
