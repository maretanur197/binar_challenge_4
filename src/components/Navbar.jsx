import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import logo from "../assets/logo.png";

const Navbar = () => {
  const [sticky, setSticky] = useState(false);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      const nav = document.querySelector("nav");
      window.scrollY > 0 ? setSticky(true) : setSticky(false);
    });
  }, []);

  return (
    <div
      className={`fixed w-full left-0 top-0 px-3 md:p-4 drop-shadow-lg z-10 
    ${sticky ? "bg-black text-white lg:h-20 items-center" : "text-white"}`}
    >
      <div className="flex items-center justify-between z-10 gap-6 cursor-pointer h-14  ">
        <div className="pl-2 flex">
          <img src={logo} alt="" className="w-10 md:w-16 lg:w-20" />
          <h4 className="md:text-4xl lg:pt-2 text-2xl font-bold text-white hover:text-red-600">
            MovieList
          </h4>
        </div>
        <div className="lg:text-lg text-white">
          <ul className="hidden md:flex font-bold gap-3 md:gap-5">
            <li
              className="hover:text-red-600 relative cursor-pointer transition-all 
               before:absolute before:-bottom-2 before:left-0 before:w-0 before:h-1 before:rounded-full before:opacity-0 before:transition-all
               before:duration-500 before:bg-red-600 hover:before:w-full hover:before:opacity-100"
            >
              <Link to="/" smooth={true} duration={500}>
                Home
              </Link>
            </li>
            <li
              className="hover:text-red-600 relative cursor-pointer transition-all 
               before:absolute before:-bottom-2 before:left-0 before:w-0 before:h-1 before:rounded-full before:opacity-0 before:transition-all
               before:duration-500 before:bg-red-600 hover:before:w-full hover:before:opacity-100"
            >
              <Link to="/ViewMovie" smooth={true} offset={-200} duration={500}>
                Movies
              </Link>
            </li>
            <li
              className="hover:text-red-600 relative cursor-pointer transition-all 
               before:absolute before:-bottom-2 before:left-0 before:w-0 before:h-1 before:rounded-full before:opacity-0 before:transition-all
               before:duration-500 before:bg-red-600 hover:before:w-full hover:before:opacity-100"
            >
              <Link to="/CatalogTv" smooth={true} offset={-200} duration={500}>
                TV
              </Link>
            </li>
            <li className="">
              <Link to="/Search" smooth={true} offset={-200} duration={500}>
                Search
              </Link>
            </li>
          </ul>
        </div>
        <div
          className={`text-gray-900 md:block hidden font-medium rounded-bl-full`}
        >
          <ul className="flex flex-row items-center gap-4">
            <li className="px-6 border border-red-600 rounded-full">
              <button className="text-red-500 text-center h-8 text-sm">
                Login
              </button>
            </li>
            <li className="px-6 bg-red-600 rounded-full text-center ">
              <button className=" text-white h-8 text-sm">Register</button>
            </li>
          </ul>
        </div>
        <div
          onClick={() => setOpen(!open)}
          className={`z-[999] ${
            open ? "text-gray-900" : "text-gray-100"
          } text-3xl md:hidden m-5`}
        >
          <ion-icon name="menu"></ion-icon>
        </div>
        <div
          className={`text-gray-900 md:hidden absolute w-2/3 h-screen px-7 py-2 font-medium bg-white top-0 ${
            open ? "right-0" : "right-[-100%]"
          }`}
        >
          <ul className="flex flex-col justify-center items-center h-full gap-10 text-lg">
            <li className="">
              <Link to="/">Home</Link>
            </li>
            <li className="">
              <Link
                to="/CatalogMovie"
                smooth={true}
                offset={-200}
                duration={500}
              >
                Movies
              </Link>
            </li>
            <li className="">
              <Link to="/CatalogTv" smooth={true} offset={-200} duration={500}>
                TV Series
              </Link>
            </li>
            <li className="">
              <Link to="/Search" smooth={true} offset={-200} duration={500}>
                Search
              </Link>
            </li>
            <li className="px-2 border border-red-600 rounded-full w-20 text-center">
              <button className="text-red-600 text-center h-7 text-sm ">
                Login
              </button>
            </li>
            <li className="px-2 bg-red-600 rounded-full text-white w-20 text-center">
              <button className=" text-center h-7 text-sm"> Register</button>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
