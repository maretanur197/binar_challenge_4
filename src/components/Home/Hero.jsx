import React, { useEffect, useState } from "react";
import axios from "axios";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Autoplay, Navigation } from "swiper";
import "swiper/css";
import "swiper/css/pagination";

import { AiFillStar } from "react-icons/ai";

const Hero = () => {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    axios
      .get(`https://api.themoviedb.org/3/discover/movie`, {
        params: {
          api_key: "24421cafe490cc00aeb89c9771051f13",
        },
      })
      .then((response) => {
        setMovies(response.data.results);
      });
  }, []);

  return (
    <div className="">
      <Swiper
        slidesPerView={1}
        spaceBetween={20}
        loop={true}
        direction="horizontal"
        autoplay={{
          delay: 5000,
          disableOnInteraction: false,
        }}
        Pagination={{
          clickable: true,
        }}
        modules={[Pagination, Autoplay]}
      >
        {movies.slice(0, 6).map((movie) => (
          <SwiperSlide className="relative overflow-hidden" key={movie.id}>
            <div className="">
              <img
                src={`https://image.tmdb.org/t/p/w500/${movie.backdrop_path}`}
                alt="movie"
                className="w-full h-64 md:h-[80vh] lg:h-[100vh] object-cover"
              />
              <div className="absolute top-12 md:top-16 lg:top-20 px-5 w-full h-full mx-auto md:p-9 lg:p-14">
                <div className="grid md:grid-cols-2 lg:grid-cols-2 gap-10 ">
                  <div>
                    <h1 className="text-2xl w-full text-white font-bold pt-7 flex md:text-5xl md:pt-10 lg:text-6xl">
                      {movie.title}
                    </h1>
                    <div className=" flex py-3">
                      <p className=" text-yellow-500 w-6 text-lg lg:pt-1">
                        <AiFillStar />
                      </p>
                      <p className="text-white text-xs lg:text-lg">
                        {movie.vote_average}
                      </p>
                    </div>
                    <p className="text-white text-[7px] w-72 md:w-full md:text-justify lg:text-base md:text-sm pb-3">
                      {movie.overview}
                    </p>
                    <button className="flex flex-rows gap-2 items-center p-2 lg:p-5 bg-red-600 text-center rounded w-28 lg:w-36 h-8 lg:h-10 text-white text-xs lg:text-sm">
                      <ion-icon name="play"></ion-icon>
                      <p> Watch Trailer</p>
                    </button>
                  </div>

                  <div className="pt-2 lg:pt-2 pl-20">
                    <img
                      src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`}
                      alt="movie"
                      className="w-52 lg:w-80 md:w-80 rounded-xl"
                    />
                  </div>
                </div>
              </div>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default Hero;
