import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import Footer from "./components/Footer";
import CatalogTv from "./pages/CatalogTv";
import ViewMovie from "./components/ViewMovie";
import Detail from "./pages/Detail";
import Search from "./pages/Search";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/ViewMovie" element={<ViewMovie />} />
          <Route path="/CatalogTv" element={<CatalogTv />} />
          <Route path="/:id" element={<Detail />} />
          <Route path="/search" element={<Search />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  );
};

export default App;
