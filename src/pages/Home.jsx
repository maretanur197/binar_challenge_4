import React from "react";
import Hero from "../components/Home/Hero";
import TvCard from "../components/Home/TvCard";
import PopularMovie from "../components/PopularMovie";

const Home = () => {
  return (
    <div>
      <Hero />
      <PopularMovie />
      <TvCard />
    </div>
  );
};

export default Home;
